import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User_DTO } from '../user-dto';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  private _user: User_DTO
  constructor(private _router: Router) { }

  ngOnInit(): void {
    this._user = JSON.parse(sessionStorage.getItem('user'));
    console.log(this._user);
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

  NavigateToStart(){
    sessionStorage.clear();
    this._router.navigateByUrl('start');
  }
}
