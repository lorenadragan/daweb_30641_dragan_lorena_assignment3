import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscriber } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User_DTO } from '../user-dto';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', Validators.required),
    userType: new FormControl('', Validators.required)
  });
  isError: boolean = false;
  errorMessage: string;

  constructor(private _router: Router,
    private _httpClient: HttpClient) {
   }

  ngOnInit(): void {
  }

  get f(){
    return this.loginForm.controls;
  }

  submit(){
    /*
    localStorage.setItem('user', JSON.stringify(user)); Then to retrieve it from the store and convert to an object again:
    var user = JSON.parse(localStorage.getItem('user')); If we need to delete all entries of the store we can simply do:
    localStorage.clear();
    */
    //operation is performed only if credentials are valid
    if(this.loginForm.status === "VALID"){
      let userType = this.loginForm.value.userType;
      let email = this.loginForm.value.email;
      let password = this.loginForm.value.password;

      if(userType === "client"){
        this._httpClient.get<User_DTO>("https://localhost:44346/api/users/email=" + email +"/password=" + password)
          .subscribe(data => {
            sessionStorage.setItem('user', JSON.stringify(data));
            this._router.navigateByUrl("client-menu")
          }, (err: HttpErrorResponse) => {
            console.log(err.message);
          })
      }
    }
  }

  resetValue(){
    this.loginForm.reset({email: '', password: ''});
    this.loginForm.value.userType = ''
  }

}
