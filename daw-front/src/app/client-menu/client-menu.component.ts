import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client-menu',
  templateUrl: './client-menu.component.html',
  styleUrls: ['./client-menu.component.css']
})
export class ClientMenuComponent implements OnInit {

  constructor(private location: LocationStrategy) {
    history.pushState(null, null, window.location.href);
    this.location.onPopState(() => {
      history.pushState(null,null, window.location.href);
    });
   }

  ngOnInit(): void {
  }

  selectLanguage(lang: string){
    lang === 'en' ? localStorage.setItem('isEnglish', 'true') : localStorage.setItem('isEnglish', 'false');
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

}
