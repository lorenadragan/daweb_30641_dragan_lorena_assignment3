import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscriber } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User_DTO } from '../user-dto';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', Validators.required),
    userType: new FormControl('', Validators.required),
    name: new FormControl('', [Validators.required, Validators.minLength(3)])
  });
  isError: boolean = false;
  errorMessage: string;

  constructor(private _httpClient: HttpClient) { }

  ngOnInit(): void {
  }

  get f(){
    return this.loginForm.controls;
  }

  submit(){
    if(this.loginForm.status === "VALID"){
      let userType = this.loginForm.value.userType;
      let email = this.loginForm.value.email
      let password = this.loginForm.value.password;
      let name = this.loginForm.value.name;

      if(userType === "client"){
        let newUser = {
          email: email,
          password: password,
          name: name
        };

        this._httpClient.post<any>("https://localhost:44346/api/users", newUser)
          .subscribe(data => {
            console.log(data);
          }, (er: HttpErrorResponse) => {
            console.log(er.message);
          })
      }
    }
  }

  resetValue(){
    this.loginForm.reset({email: '', password: '', name: ''});
    this.loginForm.value.userType = ''
  }

}
