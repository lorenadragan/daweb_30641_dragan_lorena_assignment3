import { Service } from "./service";

export class User_DTO {
  client_id: number;
  email: string;
  password: string;
  user_id: number;
  name: string;
  services: Service[] = [];

  constructor(client_id?: number, email?: string, password?: string, user_id?: number, name?: string, services?: Service[]){
    this.client_id = client_id;
    this.email = email;
    this.password = password;
    this.user_id = user_id;
    this.name = name;
    this.services = services;
  }
}
