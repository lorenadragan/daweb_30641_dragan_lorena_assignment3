import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { ClientMenuComponent } from './client-menu/client-menu.component';
import { ContactComponent } from './contact/contact.component';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { RegisterComponent } from './register/register.component';
import { ServicesComponent } from './services/services.component';
import { StartPageComponent } from './start-page/start-page.component';
import { TeamComponent } from './team/team.component';


const routes: Routes = [
  { path: '', redirectTo:'/start', pathMatch:'full'},
  { path: 'start', component: StartPageComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'client-menu', component: ClientMenuComponent,
  children: [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomePageComponent},
    { path: 'despre-noi', component: AboutUsComponent},
    { path: 'medici', component: TeamComponent},
    { path: 'servicii-tarife', component: ServicesComponent},
    { path: 'contact', component: ContactComponent},
    { path: 'profile', component: ProfilePageComponent},
    { path: 'create-appointment', component: CreateAppointmentComponent},
    { path: 'noutati', component: NewsPageComponent}
  ]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
