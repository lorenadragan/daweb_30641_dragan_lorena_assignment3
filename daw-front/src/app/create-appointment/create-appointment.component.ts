import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Appointment_DTO } from '../appointment-dto';
import { User_DTO } from '../user-dto';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { Doctor } from '../doctor';
@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.css']
})
export class CreateAppointmentComponent implements OnInit {

  user: User_DTO;
  headElements = ["Id Programare", "Doctor Name", "Date"];
  doctorsHeadElements = ["Id", "Doctor Name"]
  programariClient: Appointment_DTO[] = [];
  doctors: Doctor[] = [];
  constructor(private _httpClient: HttpClient,
    private datePipe: DatePipe) { }

  ngOnInit(): void {
    //get current user
    this.user = JSON.parse(sessionStorage.getItem('user'));

    //get all appointments
    this._refreshAppointments();

    //get all doctors
    this._getDoctors();
  }

  private _getDoctors(){
    this._httpClient.get<Doctor[]>("https://localhost:44346/api/doctors")
    .subscribe(data =>{
      this.doctors = data;
    }, (err: HttpErrorResponse) => {
      console.log(err.message);
    })
  }

  private _refreshAppointments(){
    this.programariClient = [];
    this._httpClient.get<Appointment_DTO[]>("https://localhost:44346/api/appointments/clientId=" + this.user.client_id)
      .subscribe(data => {
        data.forEach(element => {
          let newApp = new Appointment_DTO(element.id, element.client_id, moment(element.date).format('YYYY-MM-DD'), element.doctor_name);
          this.programariClient.push(newApp);
        });
      }, (err: HttpErrorResponse) => {
        console.log(err.message);
      })
  }
  DeleteAppointment(el: Appointment_DTO){
    this._httpClient.delete<any>("https://localhost:44346/api/appointments/" + el.id)
      .subscribe((data) => {
        this._refreshAppointments();
      }, (err: HttpErrorResponse) => {
        console.log(err);
      })
  }
  CreateAppointment(el: Doctor){
    let date = (<HTMLInputElement>document.getElementById('date')).value;
    let newApp = {
      medicId:el.id,
      date:date,
      clientId: this.user.client_id
    }


    this._httpClient.post("https://localhost:44346/api/appointments", newApp)
      .subscribe((data) => {
        this._refreshAppointments();
      }, (err: HttpErrorResponse) => {
        console.log(err.message);
      })
  }

  CreateAppointmentEngleza(el: Doctor) {
    let date = (<HTMLInputElement>document.getElementById('date_en')).value;
    let newApp = {
      medicId:el.id,
      date:date,
      clientId: this.user.client_id
    }


    this._httpClient.post("https://localhost:44346/api/appointments", newApp)
      .subscribe((data) => {
        this._refreshAppointments();
      }, (err: HttpErrorResponse) => {
        console.log(err.message);
      })
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

}
