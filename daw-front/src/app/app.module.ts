import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { StartPageComponent } from './start-page/start-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ClientMenuComponent } from './client-menu/client-menu.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { ServicesComponent } from './services/services.component';
import { TeamComponent } from './team/team.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import {DatePipe} from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    StartPageComponent,
    HomePageComponent,
    ClientMenuComponent,
    AboutUsComponent,
    ContactComponent,
    NewsPageComponent,
    ServicesComponent,
    TeamComponent,
    ProfilePageComponent,
    CreateAppointmentComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
