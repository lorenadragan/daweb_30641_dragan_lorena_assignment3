import { LocationStrategy } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Service } from '../service';
import { User_DTO } from '../user-dto';
@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  user: User_DTO;
  headElements = ["Id", "Name", "Price"];
  dbServices: Service[] = [];
  serviciiClienti: Service[] = [];
  constructor(private location: LocationStrategy,
    private _httpClient: HttpClient) {
    // history.pushState(null, null, window.location.href);
    //   this.location.onPopState(() => {
    //     history.pushState(null,null, window.location.href);
    //   });
  }
  file: any;
  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('user'));
    console.log(this.user);
    this.serviciiClienti = this.user.services;
    //get all services
    this._httpClient.get<Service[]>("https://localhost:44346/api/services/")
      .subscribe(data =>{
        this.dbServices = data;
      },(err: HttpErrorResponse) => {
        console.log(err.message);
      })

  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

  UpdatePersonalInformation(){
    var name = (<HTMLInputElement>document.getElementById('name')).value;
    var email = (<HTMLInputElement>document.getElementById('email')).value;
    var password = (<HTMLInputElement>document.getElementById('password')).value;
    var clientToUpdate = {
      name: name
    };
    this._httpClient.put<string>("https://localhost:44346/api/clients/" + this.user.client_id, clientToUpdate)
      .subscribe(data => {
        this.user["name"]=name;
        console.log(data);
        sessionStorage.setItem('user', JSON.stringify(this.user));
      }, (err: HttpErrorResponse) => {
        console.log(err.message)
      });
    var userToUpdate = {
      email: email,
      password: password
    };
    this._httpClient.put<string>("https://localhost:44346/api/users/" + this.user.user_id, userToUpdate)
      .subscribe(data => {

        this.user["email"] = email;
        this.user["password"] = password;
        sessionStorage.setItem('user', JSON.stringify(this.user));
      }, (err:HttpErrorResponse) => {
        console.log(err.message);
      });
  }

  UpdatePersonalInformationEngleza(){
    var name = (<HTMLInputElement>document.getElementById('name_en')).value;
    var email = (<HTMLInputElement>document.getElementById('email_en')).value;
    var password = (<HTMLInputElement>document.getElementById('password_en')).value;
    var clientToUpdate = {
      name: name
    };
    this._httpClient.put<string>("https://localhost:44346/api/clients/" + this.user.client_id, clientToUpdate)
      .subscribe(data => {
        this.user["name"]=name;
        console.log(data);
        sessionStorage.setItem('user', JSON.stringify(this.user));
      }, (err: HttpErrorResponse) => {
        console.log(err.message)
      });
    var userToUpdate = {
      email: email,
      password: password
    };
    this._httpClient.put<string>("https://localhost:44346/api/users/" + this.user.user_id, userToUpdate)
      .subscribe(data => {

        this.user["email"] = email;
        this.user["password"] = password;
        sessionStorage.setItem('user', JSON.stringify(this.user));
      }, (err:HttpErrorResponse) => {
        console.log(err.message);
      });
  }

  AdaugaServiciu(el: Service) {
    var request = "https://localhost:44346/api/usersServices/clientId=" + this.user.client_id +"/serviceId=" + el.id;
    this._httpClient.post(request,"")
      .subscribe(data => {
        console.log(data);
        this.user.services.push(el);
        sessionStorage.setItem('user', JSON.stringify(this.user));
      }, (err: HttpErrorResponse) =>{
        console.log(err.message);
      })
  }

  StergeServiciu(){
    var serviciu_id = (<HTMLInputElement>document.getElementById('serviciu_id')).value;
    var request = "https://localhost:44346/api/usersServices/delete/clientId=" + this.user.client_id +"/serviceId=" + serviciu_id;
    this._httpClient.delete(request)
      .subscribe(data => {
        this.user.services = this.user.services.filter(item => item.id !== +serviciu_id);
        sessionStorage.setItem('user', JSON.stringify(this.user));
        this.serviciiClienti = this.user.services;
      }, (err: HttpErrorResponse) =>{
        console.log(err);
      })
  }

  StergeServiciuEngleza(){
    var serviciu_id = (<HTMLInputElement>document.getElementById('serviciu_id_en')).value;
    var request = "https://localhost:44346/api/usersServices/delete/clientId=" + this.user.client_id +"/serviceId=" + serviciu_id;
    this._httpClient.delete(request)
      .subscribe(data => {
        this.user.services = this.user.services.filter(item => item.id !== +serviciu_id);
        sessionStorage.setItem('user', JSON.stringify(this.user));
        this.serviciiClienti = this.user.services;
      }, (err: HttpErrorResponse) =>{
        console.log(err);
      })
  }
  IncarcareGDPR(){}

  changeFile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

uploadFile(event) {
  console.log(event)
    if (event.target.value) {
      this.file = event.target.files[0];
      const type = this.file.type;
      this.changeFile(this.file).then((base64: string): any => {
          // console.log(base64);
          // this.fileBlob = this.b64Blob([base64], type);
          // console.log(this.fileBlob)
          sessionStorage.setItem('user_gdpr', this.file);
      });
  } else alert('Nothing')
}

}
